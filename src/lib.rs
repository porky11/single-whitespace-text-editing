#![deny(missing_docs)]

//! A library for text editing operations with single whitespace handling.
//!
//! This library provides the `SingleWhitespaceEditAction` enum and its associated functions for performing text editing
//! operations based on the input character. The library ensures that there is always exactly one
//! whitespace character in a row, automatically removing any additional whitespace when necessary.
//!
//! A library for text editing operations with single whitespace handling.
//!
//! This library provides the `SingleWhitespaceEditAction` enum and its associated functions for performing text editing
//! operations based on the input character. The library ensures that there is always exactly one
//! whitespace character in a row, automatically removing any additional whitespace when necessary.
//!
//! The following operations are supported:
//!
//! - Most common characters: Adds a character at the cursor position.
//! - Newline or carriage return: No operation is performed.
//! - Whitespace characters (except tab): Adds a single space at the cursor position.
//! - Delete ('\x7F'): Removes the character or word forward from the cursor position.
//! - Backspace ('\x08'): Removes the character or word backward from the cursor position.
//! - Any other character: No operation is performed.
//!
//! # Examples
//!
//! ## Adding a Character
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("Hello World").unwrap();
//! let mut cursor = 11;
//! let edit_op = SingleWhitespaceEditAction::new('!', false).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "Hello World!");
//! ```
//!
//! ## Adding a Space
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("HelloWorld").unwrap();
//! let mut cursor = 5;
//! let edit_op = SingleWhitespaceEditAction::new(' ', false).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "Hello World");
//! ```
//!
//! ## Pressing Space Before a Space
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("Hello World").unwrap();
//! let mut cursor = 5;
//! let edit_op = SingleWhitespaceEditAction::new(' ', false).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "Hello World");
//! assert_eq!(cursor, 6);
//! ```
//!
//! ## Removing multiple Characters with Backspace
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("Hello World!").unwrap();
//! let mut cursor = 6;
//! let edit_op = SingleWhitespaceEditAction::new('\x08', true).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "World!");
//! ```
//!
//! ## Removing a Character, Resulting in Double Spaces
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("Hello , World!").unwrap();
//! let mut cursor = 7;
//! let edit_op = SingleWhitespaceEditAction::new('\x08', false).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "Hello World!");
//! ```
//!
//! ## Removing a Character with Delete
//! ```
//! use std::str::FromStr;
//!
//! use single_whitespace_text_editing::SingleWhitespaceEditAction;
//! use text_editing::TextLine;
//!
//! let mut text = TextLine::from_str("Hello World!").unwrap();
//! let mut cursor = 5;
//! let edit_op = SingleWhitespaceEditAction::new('\x7F', false).unwrap();
//! edit_op.apply(&mut text, &mut cursor);
//! assert_eq!(text.as_str(), "HelloWorld!");
//! ```

use text_editing::TextLine;

#[derive(Clone, Copy)]
enum EditAction {
    AddChar(char),
    Custom(fn(&mut TextLine, &mut usize) -> bool),
}

/// Represents text editing operations.
#[derive(Clone, Copy)]
pub struct SingleWhitespaceEditAction {
    action: EditAction,
}

fn finish_whitespace(text: &mut TextLine, cursor: usize) {
    if (cursor < text.len() && text.char_at(cursor).is_whitespace())
        && (cursor == 0 || text.char_at(cursor - 1).is_whitespace())
    {
        text.remove(cursor);
    }
}

impl SingleWhitespaceEditAction {
    /// Creates a new `SingleWhitespaceEditAction` instance based on the provided character and skip flag.
    ///
    /// This function returns a `SingleWhitespaceEditAction` enum value that performs the appropriate text editing
    /// operation based on the input character.
    ///
    /// The supported operations include adding a character, adding a space, and
    /// removing a character or word forward or backward.
    /// If the input character is not supported, `None` is returned.
    ///
    /// # Arguments
    /// * `character`: The input character to determine the editing operation.
    /// * `skip`: A boolean flag indicating whether to skip over whitespace when removing characters. (Usually if the user holds `Ctrl`)
    ///
    /// # Returns
    /// * `Some(SingleWhitespaceEditAction)`: A `SingleWhitespaceEditAction` that performs the editing operation.
    /// * `None`: If the input character is not supported.
    pub fn new(character: char, skip: bool) -> Option<Self> {
        fn add_space(text: &mut TextLine, cursor: &mut usize) -> bool {
            let mut result = false;
            if *cursor != 0 && text.char_at(*cursor - 1) != ' ' {
                if *cursor == text.len() || text.char_at(*cursor) != ' ' {
                    text.insert(*cursor, ' ');
                    result = true;
                }
                *cursor += 1;
            }
            result
        }
        fn remove_forward(text: &mut TextLine, cursor: &mut usize) -> bool {
            let removed = text.remove_forward(*cursor);
            if removed {
                finish_whitespace(text, *cursor);
            }
            removed
        }
        fn remove_backward(text: &mut TextLine, cursor: &mut usize) -> bool {
            let removed = text.remove_backward(cursor);
            if removed {
                finish_whitespace(text, *cursor);
            }
            removed
        }
        fn remove_forward_skip(text: &mut TextLine, cursor: &mut usize) -> bool {
            let removed = text.remove_forward_skip(*cursor);
            if removed {
                finish_whitespace(text, *cursor);
            }
            removed
        }
        fn remove_backward_skip(text: &mut TextLine, cursor: &mut usize) -> bool {
            let removed = text.remove_backward_skip(cursor);
            if removed {
                finish_whitespace(text, *cursor);
            }
            removed
        }

        Some(Self {
            action: match character {
                '\n' | '\r' => None?,
                '\x08' => EditAction::Custom(if skip {
                    remove_backward_skip
                } else {
                    remove_backward
                }),
                '\x7F' => EditAction::Custom(if skip {
                    remove_forward_skip
                } else {
                    remove_forward
                }),
                c if c.is_whitespace() && c != '\t' => EditAction::Custom(add_space),
                c if !skip && !c.is_control() => EditAction::AddChar(c),
                _ => None?,
            },
        })
    }

    /// Applies the text editing operation to the given `TextLine` and cursor position.
    #[inline]
    pub fn apply(self, text: &mut TextLine, cursor: &mut usize) -> bool {
        use EditAction::*;
        match self.action {
            AddChar(c) => {
                text.add_char(cursor, c);
                true
            }
            Custom(f) => f(text, cursor),
        }
    }
}
