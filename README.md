# Single Whitespace Text Editing

This crate provides utilities for a special editing style, where the only allowed whitespace is the space, and it's only allowed once at a time.

## Features

- Ensures that there is always exactly one whitespace character in a row, automatically removing any additional whitespace when necessary.
- Provides an enum `SingleWhitespaceEditAction` and its associated functions for performing text editing operations based on the input character.
- Offers a simple and efficient way to handle text editing operations with single whitespace handling.

## Dependencies

This crate depends on the `text-editing` crate for text manipulation functionality.
